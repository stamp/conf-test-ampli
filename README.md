STAMP Test Configuration Amplifier
==================================
STAMP Test Configuration Amplifier is a set of microservices to amplify your Docker files used as test configurations. Starting from a single Docker file, STAMP will generate variations usable as new test configuration in your CD pipeline.
## License
STAMP Test Configuration Amplifier is freely available under the [LGPL license](LICENSE).